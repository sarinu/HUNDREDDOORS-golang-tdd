package hiker_test

import (
	"hundredDoors/hiker"
	"reflect"
	"testing"
)

func TestToggleDoorsShouldReturnAllTrueWhenInputIsZero(t *testing.T) {
	state := hiker.ToggleDoors(0)

	expectedState := []bool{}
	for i := 0; i < 100; i++ {
		expectedState = append(expectedState, true)
	}

	if !reflect.DeepEqual(state, expectedState) {
		t.Errorf("'%+v' should be '%+v'", state, expectedState)
	}
}

func TestToggleDoorsShouldReturnFalseEverySecondOfSliceWhenInputIsOne(t *testing.T) {
	state := hiker.ToggleDoors(1)

	expectedState := []bool{}
	for i := 0; i < 100; i++ {
		if (i+1)%2 == 0 {
			expectedState = append(expectedState, false)
		} else {
			expectedState = append(expectedState, true)
		}
	}

	if !reflect.DeepEqual(state, expectedState) {
		t.Errorf("'%+v' should be '%+v'", state, expectedState)
	}
}

func TestToogleDoorsShouldReturnReflectValueEveryThirdOfSliceWhenInputIsTwo(t *testing.T) {
	state := hiker.ToggleDoors(2)

	expectedState := []bool{}
	for i := 0; i < 100; i++ {
		if (i+1)%2 == 0 {
			expectedState = append(expectedState, false)
		} else {
			expectedState = append(expectedState, true)
		}
	}

	for j := 0; j < 100; j++ {
		if (j+1)%3 == 0 {
			expectedState[j] = !expectedState[j]
		}
	}

	if !reflect.DeepEqual(state, expectedState) {
		t.Errorf("'%+v' should be '%+v'", state, expectedState)
	}

}
