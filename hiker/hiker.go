package hiker

func ToggleDoors(round int) []bool {
	state := createDefaultState()

	for i := 0; i <= round; i++ {
		for j := 0; j < 100; j++ {
			if (j+1)%(i+1) == 0 {
				state[j] = !state[j]
			}
		}
	}

	return state
}

func createDefaultState() []bool {
	state := []bool{}

	for door := 0; door < 100; door++ {
		state = append(state, false)
	}

	return state
}
